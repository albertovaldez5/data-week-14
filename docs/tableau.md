# Introduction

Tableau makes it easier to deal with almost any source of data and visualizing it on screen.<sup><a id="fnr.1" class="footref" href="#fn.1" role="doc-backlink">1</a></sup>

Tableau is used to present the data in a way it can be used for stakeholders to make decisions and answer questions.

We need to think critically about which questions are the most important for our analysis. This is an important skill to master.

Once you know the questions you want to ask, the next thing you need to think about is how your audience will perceive the data you present. You want to share your findings in a way that reduces your personal bias and accurately represents what the data is saying.

Tableau allows us to compare data from different tables<sup><a id="fnr.2" class="footref" href="#fn.2" role="doc-backlink">2</a></sup>. We need a value that is present on both tables. If we don&rsquo;t have it is better to either process the data beforehand or use one of the builtin datases.


# The Tableau Workflow

We first load our data by making a connection to a local file or a remote source.

![img](../resources/screenshots/000.jpeg)

The first step after loading the data is to select a table from the `Files` and drag it into the main window. We can make joins of our tables if we need to include more by selecting `New Union`.

![img](../resources/screenshots/001.jpeg)


## Filtering Data

Before starting visualization we can filter the table with the top right button.

![img](../resources/screenshots/002.jpeg)


## Worksheets

Once the data is ready we can move on to our sheet1 at the bottom left.

![img](../resources/screenshots/003.jpeg)


## Visualization

For visualizing data, we drag either a `measure` or a `table` from the left side column in our worksheet panel.

![img](../resources/screenshots/004.jpeg)

In this case we used the dropdown in the column to change its representation from average to dimension.

Now we can refer to the measurements and add them as row.

![img](../resources/screenshots/005.jpeg)

Then we will go over for another column, in this case the `Gender` value.

![img](../resources/screenshots/007.jpeg)

Once we drag the Gender to the rows, another graph is created.

![img](../resources/screenshots/008.jpeg)

We can add even more graphs by dragging more data to the fields.

![img](../resources/screenshots/009.jpeg)


## Next Sheet

We are doing another analysis so we move on to create a new sheet and give it a name.

![img](../resources/screenshots/011.jpeg)

We will add the measure with (COUNT) in its name and put it in our columns.

Then neighborhood and no-show to the row fields. Then we can drag the same measure we used in columns to the `Marks` panel on the left side of the visualization. After we select the value `Detail` from the dropdown, we will get the text of the measurement displayed in the graph.

![img](../resources/screenshots/012.jpeg)


## Dates

We can change how dates are displayed by clicking the dropdown menu in our column field and selecting `Week Number` under `more`.

![img](../resources/screenshots/013.jpeg)

Once we added the (COUNT) measurement to the rows, we can change the visualization type on the right side of the window.

![img](../resources/screenshots/014.jpeg)


## Customization

We can transform our following charts into more detailed ones by adding more measurements and values to the Marks panel in the left side of our graphs.

![img](../resources/screenshots/015.jpeg)

This is the result after we set a variable value to the color property, giving us a gradient.

![img](../resources/screenshots/016.jpeg)


## Multiple Marks

In case of wanting multiple Marks, we can add two columns per row, which will give us a result like this one.

![img](../resources/screenshots/017.jpeg)

In case of wanting multiple parameters applied to one Mark, we can chose a graph style that fits the data we want to visualizze better, in this case a bubble chart with the `Competitor Name` value being a dimension, so we get all the data points in the chart and we get some information when hovering each one.

![img](../resources/screenshots/018.jpeg)


## Calculated Fields

In case we want to create more nuanced visualization like displaying the threshold of data when they pass certain values, we can create a calculation in the `Analysis` menu.

![img](../resources/screenshots/019.jpeg)

Now we create a conditional to check for thresholds in our data.

![img](../resources/screenshots/020.jpeg)

Then we can apply the calculation to the color property in the Mark panel.

![img](../resources/screenshots/021.jpeg)

We can change the colors in the top right.

![img](../resources/screenshots/022.jpeg)


# Map Charts


## Maps and Location

Whenever we work with coordinates, Tableau will display the chart as a map by default most of the time.

![img](../resources/screenshots/025.jpeg)

We can visualize our data by dragging more values into the `Marks` color and details.

![img](../resources/screenshots/026.jpeg)

Whenever using ZIP codes or other country dependent values, we can chose `Edit Locations..` under `Map` to make sure the country matches the data.

![img](../resources/screenshots/027.jpeg)


## Using Label Values

If we drag labels into color we&rsquo;ll get different sections of different colors per label.

![img](../resources/screenshots/028.jpeg)

However, if we drag the same value to the rows, we&rsquo;ll get multiple axes which we can sort the whole data by, using the small chart with arrow icon.

![img](../resources/screenshots/029.jpeg)


## Map Layers

When dealing with data from many locations, we can filter the country to make sure the data fits in the map. In this case we dragged `State` into colors and they are displayed as circles.

![img](../resources/screenshots/030.jpeg)

However, if we change the Marks to `Map`, the values will fit the map layout.

![img](../resources/screenshots/032.jpeg)

We can get binary coloring by creating a `Calculated Field`.

![img](../resources/screenshots/033.jpeg)

Finally, if we go to `Background Layers...` under `Map`, we can find varied data that Tableau includes by default, this includes population, area codes, metro boundaries, etc.

![img](../resources/screenshots/034.jpeg)

If we want to compare Tableau&rsquo;s data with our own we can add background layer and then include our data as a circle.

![img](../resources/screenshots/035.jpeg)

Then we can use stepped colors to get more stark differences between values.

![img](../resources/screenshots/036.jpeg)


# Dashboards


## Basics

Dashboards in Tableau are simply collections of worksheets. You can arrange the worksheets however you like within the dashboard. Dashboards allow you to simultaneously view multiple worksheets. When you modify a worksheet, it automatically updates the dashboard.

Most of the time, you&rsquo;ll add worksheets to your dashboard, but occasionally you may need to add links or photos. For example, you may want to add your company logo at the top of a worksheet, or add a link to an external website.

We can improve dashboard performance by taking a few actions like optimizing calculated fields, filtering data<sup><a id="fnr.3" class="footref" href="#fn.3" role="doc-backlink">3</a></sup>.


## Dashboard Workflow

We need to create the worksheets first. In this case we are creating `groups` from our data that are related to the original rows.

![img](../resources/screenshots/037.jpeg)

We will create different groups by Region manually.

![img](../resources/screenshots/038.jpeg)

The groups are integrated with our original data.

![img](../resources/screenshots/040.jpeg)

Then we can create a dashboard using the second icon on the top left and drag all the worksheets that appear on the left column to the dashboard.

![img](../resources/screenshots/041.jpeg)


## Actions and Filters

We can create an action for creating reactions to user interaction.

![img](../resources/screenshots/042.jpeg)

For example, a filter that uses the selected data as a criteria. So when we select a group, the ungrouped worksheets are ordered.

![img](../resources/screenshots/043.jpeg)

Here we select one of the groups and the other sheets change.

![img](../resources/screenshots/044.jpeg)

For comparison, here is the other group with the same data.

![img](../resources/screenshots/045.jpeg)


## Stories

A story is a set of charts and dashboards that are sorted and pasted together so we can explore them and interact with them. Stories do this by sequencing visualizations to help the audience understand the bigger picture.

When creating Tableau stories, or data visualizations in general, there&rsquo;s a general process that should be followed. You can use this process for most visualizations you&rsquo;ll create.

1.  Select your questions. During this step, you&rsquo;ll consider which results you want to share with your audience. What do they want to see? How can we use that information to make their decision-making process easier?
2.  Execute independent research. You&rsquo;ll need to look at other relevant pieces of information to build a bigger picture. Search other sources to find information that will make your visualization more powerful.
3.  Craft your Tableau story. This is when you create your story, primarily from worksheets and other visuals, with descriptions for each of them.
4.  Create a written analysis. The written analysis is intended to provide additional insight into what we&rsquo;re trying to convey to our audience. This is a good place to add extra detail so that everyone can get on the same page.

## Footnotes

<sup><a id="fn.1" class="footnum" href="#fnr.1">1</a></sup> <https://help.tableau.com/current/pro/desktop/en-us/dashboards_best_practices.htm>

<sup><a id="fn.2" class="footnum" href="#fnr.2">2</a></sup> <https://help.tableau.com/current/pro/desktop/en-us/joining_tables.htm>

<sup><a id="fn.3" class="footnum" href="#fnr.3">3</a></sup> <https://www.tableau.com/about/blog/2016/1/5-tips-make-your-dashboards-more-performant-48574>
